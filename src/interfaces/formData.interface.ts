import { IContainerItem } from './containerItem.interface';
import { IItem } from './item.interface';

export interface FormData {
  username: string;
  api_key: string;
  bins: IContainerItem[];
  items: IItem[];
  params: Object;
}
