export interface IBins {
  d: number;
  gross_weight: number;
  h: number;
  id: number;
  order_id: number;
  stack_height: number;
  used_space: number;
  used_weight: number;
  w: number;
  weight: number;
}

export interface IResponseData {
  id: string;
  errors: string[];
  status: number;
  bins_packed: [
    {
      bin_data: IBins;
      items: [
        {
          d: number;
          h: number;
          id: number;
          w: number;
          wg: number;
        }
      ];
      not_packed_items: [
        {
          id: string;
        }
      ];
      image_complete: string;
    }
  ];
  not_packed_items: IBins[];
}
