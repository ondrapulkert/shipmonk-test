export interface IContainerItem {
    w: string;
    h: string;
    d: string;
    id?: string;
    max_wg: string;
}