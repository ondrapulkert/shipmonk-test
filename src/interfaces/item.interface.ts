export interface IItem {
    w: string;
    h: string;
    d: string;
    q: string;
    vr: string;
    id?: string;
    wg: string;
}