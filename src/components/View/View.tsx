import React, { useContext } from 'react';
import { AppContext } from '../../App';
import { ViewContainer } from './styled';
import { Wrapper } from '../../styles/globalStyles';

const View: React.FC<{}> = () => {
  const { data } = useContext(AppContext);
  if (!data) {
    return null;
  }

  return (
    <ViewContainer>
      <Wrapper>
        <h3>
          Packed items: <span>{data.bins_packed[0].items.length}</span>
        </h3>
        <h3>
          Space taken: <span>{data.bins_packed[0].bin_data.used_space} %</span>
        </h3>
        <h3>
          Space left: <span>{100 - data.bins_packed[0].bin_data.used_space} %</span>
        </h3>
        <h3>
          Weight taken: <span>{data.bins_packed[0].bin_data.used_weight} %</span>
        </h3>
        <h3>
          Weight left: <span>{100 - data.bins_packed[0].bin_data.used_weight} %</span>
        </h3>
        {data.bins_packed[0].not_packed_items.length > 0 && (
          <h3>
            Not packed items: <span>{data.bins_packed[0].not_packed_items.length}</span>
          </h3>
        )}
        {data.bins_packed[0].image_complete &&
            <img src={data.bins_packed[0].image_complete} alt="" />
        }
      </Wrapper>
    </ViewContainer>
  );
};

export default View;
