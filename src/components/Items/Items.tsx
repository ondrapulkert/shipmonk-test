import React from 'react';
import { FieldErrors } from 'react-hook-form';
import { FormField, FormRow } from '../Form/styles';
import Input from '../Form/Input/Input';
import { IItem } from '../../interfaces/item.interface';
import { Button } from '../../styles/globalStyles';

interface ItemsProps {
  register: any;
  errors: FieldErrors<any>;
  fields: any;
  onRemoveItem: (index: number) => void;
}

const Items: React.FC<ItemsProps> = ({ register, errors, fields, onRemoveItem }) => {
  return (
    <div>
      <h2>Items</h2>
      {fields.map((item: IItem, index: number) => (
        <FormRow key={item.id}>
          <FormField>
            <Input
              error={errors?.['items']?.[index]?.w}
              defaultValue={item.w}
              register={register({ required: true })}
              name={`items[${index}].w`}
              placeholder="10"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['items']?.[index]?.h}
              defaultValue={item.h}
              register={register({ required: true })}
              name={`items[${index}].h`}
              placeholder="10"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['items']?.[index]?.d}
              defaultValue={item.d}
              register={register({ required: true })}
              name={`items[${index}].d`}
              placeholder="10"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['items']?.[index]?.wg}
              defaultValue={item.wg}
              register={register({ required: true })}
              name={`items[${index}].wg`}
              placeholder="0"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['items']?.[index]?.q}
              defaultValue={item.q}
              register={register({ required: true })}
              name={`items[${index}].q`}
              placeholder="0"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['items']?.[index]?.vr}
              defaultValue={item.vr}
              register={register({ required: true })}
              name={`items[${index}].vr`}
              placeholder="1"
            />
          </FormField>
          {fields.length > 1 && (
            <Button buttonStyle="delete" onClick={() => onRemoveItem(index)}>
              Delete
            </Button>
          )}
        </FormRow>
      ))}
    </div>
  );
};

export default Items;
