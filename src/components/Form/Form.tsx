import React, { useContext } from 'react';
import { useForm, useFieldArray } from 'react-hook-form';
import Container from '../Container/Container';
import Items from '../Items/Items';
import { FormData } from '../../interfaces/formData.interface';
import { FormContainer } from './styles';
import { IItem } from '../../interfaces/item.interface';
import { IContainerItem } from '../../interfaces/containerItem.interface';
import { AppContext } from '../../App';
import { Button } from '../../styles/globalStyles';

const Form: React.FC<{}> = () => {
  const { setData } = useContext(AppContext);

  const methods = useForm<FormData>({
    defaultValues: {
      bins: [{ w: '10', h: '10', d: '10', max_wg: '0' }],
      items: [{ w: '4', h: '5', d: '6', wg: '0', vr: '1', q: '5' }],
    },
  });
  const { register, control, handleSubmit, errors } = methods;

  const items = useFieldArray({
    control,
    name: 'items',
  });

  const bins = useFieldArray({
    control,
    name: 'bins',
  });

  const onSubmit = async (data: { bins: IContainerItem[]; items: IItem[] }) => {
    const requestData: FormData = {
      username: 'ondrapulkert',
      api_key: '965241dffd73500f5c19e2cf43b0b736',
      bins: data.bins,
      items: data.items,
      params: {
        images_background_color: '246,246,246',
        images_bin_border_color: '59,59,59',
        images_bin_fill_color: '246,246,246',
        images_item_border_color: '255,255,255',
        images_item_fill_color: '70,161,71',
        images_item_back_border_color: '215,103,103',
        images_sbs_last_item_fill_color: '70,161,71',
        images_sbs_last_item_border_color: '145,133,133',
        images_width: 100,
        images_height: 100,
        images_source: 'file',
        images_sbs: 1,
        stats: 1,
        item_coordinates: 1,
        images_complete: 1,
        images_separated: 1,
      },
    };

    const settings = {
      method: 'POST',
      body: JSON.stringify(requestData),
    };
    try {
      const response = await fetch('http://eu.api.3dbinpacking.com/packer/pack', settings);
      const data = await response.json();
      setData(data.response);
    } catch (e) {
      return e;
    }
  };

  const addItem = () => {
    items.append({ w: '4', h: '5', d: '6', wg: '0', vr: '1', q: '5' });
  };

  const removeItem = (index: number) => {
    items.remove(index);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContainer>
        <Container register={register} errors={errors} fields={bins.fields} />
        <Items onRemoveItem={removeItem} register={register} errors={errors} fields={items.fields} />
      </FormContainer>

      <Button buttonStyle="secondary" type="button" onClick={addItem}>
        Add Item
      </Button>
      <Button buttonStyle="primary" type="submit">
        Pack
      </Button>
    </form>
  );
};

export default Form;
