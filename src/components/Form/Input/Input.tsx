import React from 'react';
import { InputWrapper } from './styles';
import { FieldError } from 'react-hook-form';

interface InputProps {
  type?: string;
  placeholder?: string;
  name: string;
  register?: any;
  id?: string;
  error?: FieldError | boolean;
  defaultValue?: string | number | undefined;
}

const Input: React.FC<InputProps> = ({ defaultValue, type = 'text', placeholder = '', name, register, id, error = false }) => {
  return (
    <InputWrapper error={error}>
      <input defaultValue={defaultValue} id={id} name={name} type={type} placeholder={placeholder} ref={register} />
    </InputWrapper>
  );
};

export default Input;
