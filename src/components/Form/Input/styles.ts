import styled from 'styled-components';
import { FieldError } from 'react-hook-form';

interface InputWrapperProps {
  error?: FieldError | boolean;
}

export const InputWrapper = styled.div<InputWrapperProps>`
  position: relative;
  width: 100%;
  height: 100%;

  input {
    color: #555;
    min-width: 50px;
    max-width: 100%;
    border: 1px solid #ccc;
    border-radius: 4px;
    background-color: #fff;
    padding: 6px 12px;
    font-size: 14px;
    letter-spacing: -0.22px;
    width: 100%;
    height: 34px;
    outline: none;
    font-weight: normal;

    ::placeholder {
      color: #5c5c5c;
      opacity: 1;
    }

    &:focus {
      color: #5c5c5c;
    }
  }

  ${({ error }) =>
    error &&
    `
    input, input:focus {
      border: 1px solid #D0021B;
    }
  `}
`;
