import styled from 'styled-components';
import media from '../../styles/media';

export const FormRow = styled.div`
  ${media.md`
    display: flex;
    align-items: center;
  `};
`;

export const FormField = styled.div`
  width: 100%;
  margin-bottom: 10px;

  ${media.md`
    margin-right: 10px;
    width: 60px;
  `};
`;

export const FormContainer = styled.div`
  ${media.md`
    display: flex;
    justify-content: space-between;
  `}
`;
