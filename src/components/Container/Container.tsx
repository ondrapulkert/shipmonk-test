import React from 'react';
import { ContainerWrapper } from './styles';
import { FieldErrors } from 'react-hook-form';
import Input from '../Form/Input/Input';
import { FormField, FormRow } from '../Form/styles';
import { IContainerItem } from '../../interfaces/containerItem.interface';

interface ContainerProps {
  register: any;
  errors: FieldErrors<any>;
  fields: any;
}

const Container: React.FC<ContainerProps> = ({ register, errors, fields }) => {
  return (
    <ContainerWrapper>
      <h2>Container</h2>
      {fields.map((item: IContainerItem, index: number) => (
        <FormRow key={`bins[${index}].id`}>
          <FormField>
            <Input
              error={errors?.['bins']?.[index]?.w}
              defaultValue={item.w}
              register={register({ required: true })}
              name={`bins[${index}].w`}
              placeholder="5"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['bins']?.[index]?.h}
              defaultValue={item.h}
              register={register({ required: true })}
              name={`bins[${index}].h`}
              placeholder="5"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['bins']?.[index]?.d}
              defaultValue={item.d}
              register={register({ required: true })}
              name={`bins[${index}].d`}
              placeholder="5"
            />
          </FormField>
          <FormField>
            <Input
              error={errors?.['bins']?.[index]?.max_wg}
              defaultValue={item.max_wg}
              register={register({ required: true })}
              name={`bins[${index}].max_wg`}
              placeholder="0"
            />
          </FormField>
        </FormRow>
      ))}
    </ContainerWrapper>
  );
};

export default Container;
