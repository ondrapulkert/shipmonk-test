import React, { useState } from 'react';
import { GlobalStyle, Page, Wrapper } from './styles/globalStyles';
import Form from './components/Form/Form';
import { IResponseData } from './interfaces/responseData.interface';
import View from './components/View/View';

export interface IAppContext {
  data: IResponseData | null;
  setData: (data: IResponseData | null) => void;
}

const appCtxDefaultValue = {
  data: null,
  setData: () => {},
};

export const AppContext = React.createContext<IAppContext>(appCtxDefaultValue);

const App: React.FC<{}> = () => {
  const [data, setData] = useState<IResponseData | null>(appCtxDefaultValue.data);

  return (
    <AppContext.Provider value={{ data, setData }}>
      <Page>
        <Wrapper>
          <h1>Single Container Packing</h1>
          <GlobalStyle />
          <Form />
        </Wrapper>
        <View />
      </Page>
    </AppContext.Provider>
  );
};

export default App;
