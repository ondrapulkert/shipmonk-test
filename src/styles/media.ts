import { css } from 'styled-components';

const breakpoints: { [key: string]: number } = {
  xxl: 1440,
  xl: 1170,
  lg: 992,
  md: 768,
  sm: 576,
  xs: 376,
};

const media = Object.keys(breakpoints).reduce((acc: { [key: string]: (styles: any, ...interpolations: any[]) => any }, label: string) => {
  const size = breakpoints[label];

  Object.assign(acc, {
    [label]: (styles: any, ...interpolations: any[]) => css`
      @media (min-width: ${size}px) {
        ${css(styles, ...interpolations)}
      }
    `,
  });

  return acc;
}, {});

export default media;
