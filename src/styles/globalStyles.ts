import styled, { createGlobalStyle } from 'styled-components';
import media from './media';

interface ButtonProps {
  buttonStyle?: any;
}

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  
  body {
    color: #5c5c5c;
    font-size: 14px;
    font-family: 'Roboto', sans-serif;
  }
  
  ul {
    list-style: none;
    padding: 0;
    margin: 0;
  }
  
  p {
    line-height: 16px;
    padding: 0 0 20px;
  }
  
  h1 {
    font-weight: 500;
    font-size: 25px;
    margin: 0 0 30px;
    
    ${media.md`
      font-size: 40px;
    `};
  }
  
  h2 {
    font-weight: 500;
    font-size: 20px;
    
    ${media.md`
      font-size: 30px;
    `};
  }
  
  h3 {
    font-weight: 500;
    
    span {
      font-weight: 400;
    }
  }
`;

export const Page = styled.div`
  max-width: 1170px;
  width: 100%;
  margin: 40px auto;
  padding: 0 20px;

  ${media.xl`
    padding: 0;
  `};
`;

export const Wrapper = styled.div`
  border: 1px solid #d5d5d5;
  background: #f6f6f6;
  width: 100%;
  padding: 30px;
`;

const handleButtonType = (buttonStyle: string) => {
  switch (buttonStyle) {
    case 'primary':
      return `color: #fff; background: #46A147; &:hover {filter: brightness(1.1);}`;
    case 'secondary':
      return `color: #46A147; border: 1px solid #46A147; background: none;`;
    case 'delete':
      return `color: #fff; background: #D0021B; margin-bottom: 30px;`;
  }
};

export const Button = styled('button')<ButtonProps>`
  border-radius: 2px;
  border: none;
  width: 100px;
  height: 50px;
  line-height: 50px;
  text-align: center;
  outline: 0;
  cursor: pointer;
  transition: all 0.2s ease-out;
  display: inline-block;
  margin-right: 10px;
  align-items: center;
  ${({ buttonStyle }) => handleButtonType(buttonStyle)};
`;
